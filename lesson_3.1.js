function palindrome(str) {
    str = str + "";
    str = str.toLowerCase().split(" ").join("");
    let length = str.length;
    if (length < 2) {return true;}
    else if (str[0] === str[length  - 1]) {
        return palindrome( str.slice(1, length  - 1));
    } 
    else{return false;}    
}

console.log(palindrome("Qwerty 122 1 Yt rew q"));
console.log(palindrome(123321));